from django.apps import AppConfig


class Mfb01Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MFB01'
