
from django.db import models
import pymssql
from dbutils.pooled_db import PooledDB
# Create your models here.
# conn = pymssql.connect(server='10.1.1.109', database='Lumitek', user='sa', password='Cz61@admin', charset="big5")
pool = PooledDB(pymssql, 5, server='10.1.1.109', database='Lumitek', user='sa', password='Cz61@admin', charset="big5")
# conn = pool.connection()
# cursor = conn.cursor()
class common():
    def permissions(self,functionName, staffNo, act):
        conn = pool.connection()
        cursor = conn.cursor()
        strRightTable = "baccount_RIGHT"
        strAccBAS = "baccount_DEPT_BAS"
        strAcc = "baccount_DEPT"
        strFuncDat = "CZLumitek"
        sqlR = cursor.execute(
		    "select A.FUNC_NAME, rightInsert, rightQuery, rightUpdate, rightDelete, "
	        "rightPrint, rightConfirm1, rightConfirm2, rightConfirm3 from ( "
			"select distinct a.FUNC_NAME, "  #群組權限
			"rightInsert, rightQuery, rightUpdate, rightDelete, "
			"rightPrint, rightConfirm1, rightConfirm2, rightConfirm3 "
			"from " + strRightTable + " a  "
			"left join " + strAccBAS + " b on b.DEP_NO = a.RGT "
			"left join " + strAcc + " c on c.DEP_NO = b.DEP_NO "
			"where a.RGT <> 0 and b.isEnable='1'"
			"and a.FUNC_NAME = N'" + strFuncDat + "." + functionName + "' "
			"and c.ID ='" + staffNo + "' "
			"union "
			"select distinct a.FUNC_NAME,  "  #個人權限
			"rightInsert, rightQuery, rightUpdate, rightDelete, "
			"rightPrint, rightConfirm1, rightConfirm2, rightConfirm3 "
			"from " + strRightTable + " a "
			"left join " + strAcc + " d on d.ID=a.ID "
			"left join " + strAccBAS + " e on d.DEP_NO=e.DEP_NO "
			"where a.RGT=0 and e.isEnable='1' "
			"and a.FUNC_NAME = N'" + strFuncDat + "." + functionName + "' "
			"and a.id='" + staffNo + "') as A "
			"order by A.FUNC_NAME")
        PermissionsList = [item for item in list(sqlR.fetchall())]
        for i in range(len(PermissionsList)):
            if PermissionsList[i][act] == "1":
                # conn.close()
                return True
        # conn.close()
        return False
class btnImport():
    def Check(self):
        conn = pool.connection()
        cursor = conn.cursor()
        sql_query = """
        select paramName,paramValue
        from GL_param 
        where paramGroup = 'MF_STOCK對應參數'
        and isEnable = 1 "
        order by orderNo
        """
        cursor.execute(sql_query)
        cursor2 = self.conn.cursor()
        sql_query = """
        select paramName,paramValue
        from GL_param
        where paramGroup = 'MF_STOCK對應參數'
        and isEnable = 1
        order by orderNo
        """
        cursor2.execute(sql_query)
        # conn.close()
        return cursor, cursor2
class btnAttachmentReload():
    def reLoad(self, applyNo, functionName):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(
            "select '0' as isSel,seqNo,applyNo,fileName,filePath,description,isEnable,sourceFile,created_by,created_on,modified_by,modified_on,'檢視' as showdata,applyFunction,applyItemNo "
            "from GL_attachments "
            "where applyNo='" + applyNo + "' "
            "and isEnable=1 "
            "and applyFunction=N'" + functionName + "' "
            "order by seqNo")
        results = []
        for i in cursor.fetchall():
            result = []
            for j in list(i):
                result.append(j)
            results.append(result)
        # conn.close()
        return results
class btnLotQueue():
    def isExist(self, lotNo, applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        rdata = cursor.execute(
            "select std.status, st.status "
            "from MF_STOCK_DETAIL std, MF_STOCK st "
            "where st.applyNo=std.applyNo "
            "and std.lotNo='" + lotNo + "' and std.applyNo='" + applyNo + "' ")
        # conn.close()
        return rdata
    def isSame(self,lotNo,custNo):
        conn = pool.connection()
        cursor = conn.cursor()
        chkLotNo = cursor.execute(
            "select lotNo "
            "from MF_STOCK_DETAIL "
            "where lotNo='" + lotNo + "' "
            "and custNo='" + custNo + "' "
            "and status not in ('CLOSE','VOID') ")
        # conn.close()
        return chkLotNo
    def upData(self,lotNo,applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        try:
            cursor.execute(
                "update MF_STOCK_DETAIL set status='QUEUE' "
                " where lotNo='" + lotNo + "' "
                "   and applyNo='" + applyNo + "' "
                "   and status='VOID'")
            cursor.execute(
                "update MF_STOCK_DETAIL_DETAIL set status='QUEUE' "
                " where lotNo='" + lotNo + "' "
                "   and applyNo='" + applyNo + "' "
                "   and status='VOID'")
            cursor.execute(
                "update MF_STOCK set status='QUEUE' "
                " where applyNO='" + applyNo + "'")
            # conn.close()
            return ""
        except:
            # conn.close()
            return "数据库updata时出错"

class btnLotVoid():
    def SPQuery(self, lotNo):
        conn = pool.connection()
        cursor = conn.cursor()
        rdata = cursor.execute(
            "select keyNo from TMP_KEYNO_LOCK_MFB02 "
            " where keyNo='" + lotNo + "' and funcName='MO_LOTNO' "
        )
        # conn.close()
        return rdata
    def isExist(self, lotNo, applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        rdata = cursor.execute(
            "select status "
            " from MF_STOCK_DETAIL "
            "where lotNo='" + lotNo + "' and applyNo='" + applyNo + "' ")
        # conn.close()
        return rdata
    def upData(self,lotNo,applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        try:
            cursor.execute(
                "update MF_STOCK_DETAIL set status='VOID' "
				" where lotNo='" + lotNo + "' "
				"   and applyNo='" + applyNo + "' "
				"   and status='QUEUE'")
            cursor.execute(
                "update MF_STOCK_DETAIL_DETAIL set status='VOID' "
				" where lotNo='" + lotNo + "' "
				"   and applyNo='" + applyNo + "' "
				"   and status='QUEUE'")
            cursor.execute(
                "update MF_STOCK set status='VOID' "
				"where applyNO='" + applyNo + "'"
				"and status='QUEUE' "
				"and not exists(select lotNo "
				"  from MF_STOCK_DETAIL "
				"  where applyNo='" + applyNo + "' and status<>'VOID') ")
            cursor.execute(
                "update MF_STOCK set status='OPEN' "
                "where applyNO='" + applyNo + "' "
                "and status='QUEUE' "
                "and not exists(select lotNo "
                "from MF_STOCK_DETAIL "
                "where applyNo='" + applyNo + "' and status='QUEUE') ")
            # conn.close()
            return ""
        except:
            # conn.close()
            return "数据库updata时出错"
class btnVoid():
    def status(self, applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(
            "select MO_applyNo, MO_status, status, D_status, DD_status "
            "  from vMF_STOCK "
            " where applyNo = '" + applyNo + "' ")
        # conn.close()
    def upData(self, applyNo):
        conn = pool.connection()
        cursor = conn.cursor()
        try:
            cursor.execute(
                "update MF_STOCK "
                "   set status='VOID' "
                " where applyNo='" + applyNo + "' ")
            cursor.execute(
                "update MF_STOCK_DETAIL "
                "   set status='VOID' "
                " where applyNo='" + applyNo + "' ")
            cursor.execute(
                "update MF_STOCK_DETAIL_DETAIL "
                "   set status='VOID' "
                " where applyNo='" + applyNo + "' ")
            cursor.execute(
                "delete FROM  RS_CT "
                " where ST_applyNo='" + applyNo + "' ")
            cursor.execute(
                "delete FROM  RS_CT_A1 "
                " where ST_applyNo='" + applyNo + "' ")
            cursor.execute(
                "delete FROM  RS_LM_CT "
                " where ST_applyNo='" + applyNo + "' ")
            # conn.close()
            return ""
        except:
            # conn.close()
            return "数据库updata时出错"
class tableQueryAll():

    def haveKey(self, tableName):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM "+tableName)
        tableKeyList = []
        for i in list(cursor.description):
            tableKeyList.append(i[0])
        # conn.close()
        return tableKeyList
    def Query(self,condition,dataIndex):              #condition1,condition2,condition3,condition4
        conn = pool.connection()
        cursor = conn.cursor()
        results = {}
        cursor.execute(
            "SELECT "
            "TOP 50000 A.applyNo "
            "FROM "
            "MF_STOCK A "
            "LEFT JOIN MF_STOCK_DETAIL_DETAIL B ON A.applyNo=B.applyNo "
            + condition +
            "ORDER BY A.created_on DESC")
        # print(cursor.fetchall()[0])
        applyNoList = list(set(item[0] for item in cursor.fetchall()))
        applyNoList = sorted(applyNoList, reverse=True)
        # print(len(applyNoList))
        if len(applyNoList) == 0:
            return {"dataSize": 0}
        results["dataSize"] = len(applyNoList) if len(applyNoList) <= 500 else 500
        applyNo = applyNoList[dataIndex]
        cursor.execute(
            "SELECT * "
            "FROM MF_STOCK "
            "WHERE applyNo='" + applyNo + "'"
        )
        # print(type(cursor.fetchall()))
        # print()
        # print("dataTime" + list(list(cursor.fetchall())[0])[2])
        tableData = list(cursor.fetchall()[0])

        form = {}
        keyList = ["modified_by", "modified_on", "created_by", "created_on", "applyNo", "orderNo", "custNo", "processType", "totWaferQty", "degree", "custSite", "issueDate", "shipSite", "custProductNo", "custProductName", "status", "remark", "CustType"]
        for i in range(1, len(tableData)):
            # print(tableData[i])
            if i == 2 or i == 4 or i == 12:
                form[keyList[i - 1]] = str(tableData[i])[:19]
            else:
                form[keyList[i-1]] = tableData[i]
        results["form"] = form

        cursor.execute(
            "SELECT orderItemNo,custSite,custProductNo,custProductName,lotNo,task,waferQty,shipSite,waferSize,reworkNo,batchNo, "
            "DueDate,fixBin,machineGroup,pageDiagonalLine,packageType,padGrade,sortFailBin,lifePickup,ESD,electricCurrent,status, "
            "projectID,approval,KEA,AProject,fEffectiveQty,fQtyLimit,sreturn,sengverify "
            "FROM MF_STOCK_DETAIL "
            "WHERE applyNo='" + applyNo + "'"
        )
        tbDetail = []
        keyList = ["orderItemNo", "custSite", "custProductNo", "custProductName", "lotNo", "task", "waferQty", "shipSite", "waferSize", "reworkNo", "batchNo", "DueDate","fixBin","machineGroup","pageDiagonalLine","packageType","padGrade","sortFailBin","lifePickup","ESD","electricCurrent","status", "projectID","approval","KEA","AProject",'fEffectiveQty',"fQtyLimit","sreturn","sengverify"]
        for i in cursor.fetchall():
            tbDetailSecond = {}
            for j in range(len(i)):
                tbDetailSecond[keyList[j]] = i[j]
            tbDetail.append(tbDetailSecond)
        results["tbDetail"] = tbDetail
        # print(list(cursor.fetchall()))
        cursor.execute(
            "SELECT lotNo,itemNo,WIPID,custProductName,laserMark,lifePickup,piece,KEA "
            "FROM MF_STOCK_DETAIL_DETAIL "
            "WHERE applyNo='" + applyNo + "'"
        )
        tbDetailDetail = []
        keyList = ["lotNo", "itemNo", "WIPID", "custProductName", "laserMark", "lifePickup", "piece", "KEA"]

        for i in cursor.fetchall():
            tbDetailDetailSecond = {}
            for j in range(len(i)):
                tbDetailDetailSecond[keyList[j]] = i[j]
            tbDetailDetail.append(tbDetailDetailSecond)
        results["tbDetailDetail"] = tbDetailDetail
        # print(cursor.fetchall())
        cursor.execute(
            "SELECT fileName "
            "FROM GL_attachments "
            "WHERE applyNo='" + applyNo + "'"
        )
        tbAttachment = []
        for i in cursor.fetchall():
            tbAttachmentSecond = {}
            for j in range(len(i)):
                tbAttachmentSecond["fileName"] = i[j]
            tbAttachment.append(tbAttachmentSecond)
        results["tbAttachment"] = tbAttachment
        # print(cursor.fetchall())
        # conn.close()
        return results
class DataAddition():
    def MF_STOCK(self, Information):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(

        )

    def MF_STOCK_DETAIL(self, Information):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(

        )

    def MF_STOCK_DETAIL_DETAIL(self, Information):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(

        )

    def GL_attachments(self, Information):
        conn = pool.connection()
        cursor = conn.cursor()
        cursor.execute(

        )
