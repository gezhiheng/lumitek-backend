# encoding:utf-8
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
import json
import os
from .models import *


def btnAttachmentReload(request):                                       # 附件表重新加载按钮
    if not request.body:
        return JsonResponse({"err": "请求体错误"})
    applyNo = json.loads(request.body)["applyNo"]
    functionName = json.loads(request.body)["functionName"]
    results = btnAttachmentReload.reLoad(applyNo, functionName)
    return JsonResponse({"results": results})

def btnLotQueue(request):                                                  # 表恢复按钮
    if not request.body:
        return JsonResponse({"err": "请求体错误"})
    user = json.loads(request.body)["user"]  # 操作人员
    applyNo = json.loads(request.body)["applyNo"]
    custNo = json.loads(request.body)["custNo"]
    functionName = json.loads(request.body)["functionName"]  # 当前表单：MFB01-晶圆进货资料单
    selectedTable = list(json.loads(request.body)["selectedTable"])  # 要更改的表数据
    if len(selectedTable) == 0:
        return JsonResponse({"err": "请先选取欲恢复的资料"})
    if not common.permissions(functionName, user, 6):
        return JsonResponse({"err": "无此权限，请确认！"})
    # 进行还原
    errMsg = ""  # 报错提示
    for i in range(len(selectedTable)):
        lotNo = selectedTable[i][4]
        returnString = doVoidQueue(lotNo, applyNo, custNo)
        if returnString != "":
            errMsg += returnString + "\n"
    if errMsg.length() > 0:
        return JsonResponse({"tip": "处理完成，但中间有发生错误，错误明细如下：\n" + errMsg})

    else:
        return JsonResponse({"tip": "处理完成!"})

def doVoidQueue(lotNo, applyNo, custNo):
    rdata = list[btnLotQueue.isExist(lotNo, applyNo).fetchall()]
    if rdata.length == 0:
        return ""
    if not rdata[0][0] == "VOID":
        return "此客户委工单:" + lotNo + ", 状态非VOID不可还原."
    elif rdata[0][1] == "VOID":
        # 整張單作廢不可只還原其中一筆, 有可能會造成同客戶訂單號多筆進貨資料, 雖無傷大雅...
        return "此进货单为整张作废，不可使用单笔还原."

    # 檢核是否有相同客戶有效委工單號
    if 0 < list(btnLotQueue.isSame(lotNo, custNo).fetchall()).length():
        return "下委工单号仍有相同客户委工单在制未出货，不可还原，请确认！"
    """ returnString = btnLotQueue.upData(lotNo, applyNo)                       upData操作影响数据库，暂时不可用
    if returnString == "":
        return ""
    else:
        return returnString
"""
def btnLotVoid(request):                                                      #  表废除按钮
    if not request.body:
        return JsonResponse({"err": "请求体错误"})
    user = json.loads(request.body)["user"]  # 操作人员
    applyNo = json.loads(request.body)["applyNo"]
    custNo = json.loads(request.body)["custNo"]
    functionName = json.loads(request.body)["functionName"]  # 当前表单：MFB01-晶圆进货资料单
    selectedTable = list(json.loads(request.body)["selectedTable"])  # 要更改的表数据
    if len(selectedTable) == 0:
        return JsonResponse({"err": "请先选取欲恢复的资料"})
    if not common.permissions(functionName, user, 6):
        return JsonResponse({"err": "无此权限，请确认！"})
    # 进行删除
    errMsg = ""  # 报错提示
    for i in range(len(selectedTable)):
        lotNo = selectedTable[i][4]
        returnString = doVoidLot(lotNo, applyNo, custNo)
        if returnString != "":
            errMsg += returnString + "\n"
    if errMsg.length() > 0:
        return JsonResponse({"tip": "处理完成，但中间有发生错误，错误明细如下：\n" + errMsg})

    else:
        return JsonResponse({"tip": "处理完成!"})
def doVoidLot(lotNo, applyNo, custNo):
    rdata = list[btnLotVoid.SPQuery(lotNo).fetchall()]
    if rdata.length != 0:
        return "此客户委工单资料另一台电脑进行中， 请确认！"
    rdata = list[btnLotVoid.isExist(lotNo, applyNo).fetchall()]
    if rdata.length == 0:
        return ""
    if not rdata[0][0] == "QUEUE":
        return "此客户委工单:" + lotNo + ", 状态非QUEUE不可还原."
    """ returnString = doVoidLot.upData(lotNo, applyNo)                       upData操作影响数据库，暂时不可用
    if returnString == "":
        return ""
    else:
        return returnString
"""
def btnVoid(request):                                                #功能表数据废除
    if not request.body:
        return JsonResponse({"err": "请求体错误"})
    user = json.loads(request.body)["user"]  # 操作人员
    applyNo = json.loads(request.body)["applyNo"]
    functionName = json.loads(request.body)["functionName"]  # 当前表单：MFB01-晶圆进货资料单
    if not common.permissions(functionName, user, 6):
        return JsonResponse({"err": "无此权限，请确认！"})
    rdata = btnVoid.status(applyNo)
    for i in rdata:
        if i[4] != "QUEUE":
            return JsonResponse({"tip": "此张进货单明细资料未完全回推，请确认!"})

    returnString = btnVoid.upData(applyNo)
    if returnString == "":
        return JsonResponse({"tip": "处理完成!"})
    else:
        return JsonResponse({"tip": returnString})
def queryAll(request):                                               # 查询按钮
    if not request.body:
        print("请求出错：没有检查到body")
        return JsonResponse({"err": "请求出错：没有检查到body"})
    requestBody = json.loads(request.body)
    try:
        dataIndex = requestBody["dataIndex"]
        # print(dataNo)
    except:
        return JsonResponse({"err": "请求体出错!"})
    requestBody = json.loads(request.body)
    tableKeyList = tableQueryAll().haveKey("MF_STOCK")
    tableKeyList2 = tableQueryAll().haveKey("MF_STOCK_DETAIL_DETAIL")
    condition = whereSplicing(requestBody, tableKeyList, tableKeyList2)
    print(condition)
    return JsonResponse(tableQueryAll().Query(condition, dataIndex))
def whereSplicing (requestBody, tableKeyList, tableKeyList2) :
    whereAddString = "WHERE "
    for i in list(requestBody):
        if str(requestBody[i]).strip() == "":
            del requestBody[i]

    for i in list(requestBody):
        if i == "startTime":
            whereAddString += "A.issueDate >= '"+requestBody["startTime"]+"' AND "
            del requestBody["startTime"]
            continue
        if i == "endTime":
            whereAddString += "A.issueDate <= '" + requestBody["endTime"] + "' AND "
            del requestBody["endTime"]
            continue
        if i in tableKeyList and requestBody[i] != "":
            whereAddString += "A." + i + "='" + requestBody[i] + "' AND "
            del requestBody[i]
    for j in list(requestBody):
        if j in tableKeyList2 and requestBody[j] != "":
            whereAddString += "B." + j + "='" + requestBody[j] + "' AND "
            del requestBody[j]
    if whereAddString == "WHERE ":
        return ""
    else:
        return whereAddString[:-4]

def dataADD():
    pass