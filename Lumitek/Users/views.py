from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from .models import *
from django.http import HttpResponse, JsonResponse
from django.core import serializers
import json, os
from django.views.decorators.csrf import csrf_exempt



def Login (request):
    if not request.body:
        return JsonResponse({"err": "权限申请过程出现错误"})
     # print(dict(json.loads(request.body)))
    staffNo = json.loads(request.body)["staffNo"]
    password = json.loads(request.body)["password"]
    listCheckh = baccount().Check(staffNo, password)
    if listCheckh == 0:
        return JsonResponse({"err": "账号密码错误"})
    else:
        listSearch = baccount().Search(staffNo)
        return JsonResponse({"staffNo": staffNo, "username": listSearch[0][0]})

# todo:建权（没有登录不能使用）
def PermissionQuery(request, id):
    FUNC_NAMES = baccount_RIGHT().Inquire(id)  #根据路径参数id查询数据
    if len(FUNC_NAMES) == 0:
        result ={"err": "权限申请错误"}
        return JsonResponse(result)
    # print(FUNC_NAMES)
    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, '.\\migrations\\FunctionList.txt')
    with open(file_path, 'r',encoding="UTF-8") as file:
        lines = file.readlines()
    filtered_lines = []
    for line in lines:
        if line.strip() in FUNC_NAMES:
            filtered_lines.append(line)#.replace("\n", "")
    result = CreateJsonStructure(filtered_lines)
    return HttpResponse(result)


def CreateJsonStructure(data):     #list数据根据‘u3000’数量分级转JSON
    result = {}
    stack = []
    for item in data:
        level = item.count('\u3000')  # 计算空格数量
        value = item.strip()  # 移除字符串两端的空白字符
        if level == 0:
            result[value] = {}
            stack = [result[value]]
        else:
            while len(stack) > level:
                stack.pop()
            stack[-1][value] = {}
            stack.append(stack[-1][value])
    return json.dumps(result, ensure_ascii=False, indent=4)
